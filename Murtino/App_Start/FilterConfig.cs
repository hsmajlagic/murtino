﻿using Boilerplate.Web.Mvc.Filters;
using NWebsec.Mvc.HttpHeaders;
using NWebsec.Mvc.HttpHeaders.Csp;
using System.Web.Mvc;

namespace Murtino
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new XContentTypeOptionsAttribute());
            filters.Add(new XDownloadOptionsAttribute());
            filters.Add(
                new XFrameOptionsAttribute()
                {
                    Policy = XFrameOptionsPolicy.Disabled
                });
            filters.Add(new RedirectToCanonicalUrlAttribute(true, true));
            //AddContentSecurityPolicyFilters(filters);
        }

        private static void AddContentSecurityPolicyFilters(GlobalFilterCollection filters)
        {
            // Content-Security-Policy - Add the Content-Security-Policy HTTP header to enable Content-Security-Policy.
            filters.Add(new CspAttribute());
            // OR
            // Content-Security-Policy-Report-Only - Add the Content-Security-Policy-Report-Only HTTP header to enable
            //      logging of violations without blocking them. This is good for testing CSP without enabling it. To
            //      make use of this attribute, rename all the attributes below to their ReportOnlyAttribute versions
            //      e.g. CspDefaultSrcAttribute becomes CspDefaultSrcReportOnlyAttribute.
            // filters.Add(new CspReportOnlyAttribute());


            // Enables logging of CSP violations. See the NWebsecHttpHeaderSecurityModule_CspViolationReported method
            // in Global.asax.cs to see where they are logged.
            filters.Add(new CspReportUriAttribute() { EnableBuiltinHandler = true });


            // default-src - Sets a default source list for a number of directives. If the other directives below are
            //               not used then this is the default setting.
            filters.Add(
                new CspDefaultSrcAttribute()
                {
                    // Disallow everything from the same domain by default.
                    None = true,
                    // Allow everything from the same domain by default.
                    // Self = true
                });


            // base-uri - This directive restricts the document base URL
            //            (See http://www.w3.org/TR/html5/infrastructure.html#document-base-url).
            filters.Add(
                new CspBaseUriAttribute()
                {
                    // Allow base URL's from example.com.
                    // CustomSources = "*.example.com",
                    // Allow base URL's from the same domain.
                    Self = false
                });
            // child-src - This directive restricts from where the protected resource can load web workers or embed
            //             frames. This was introduced in CSP 2.0 to replace frame-src. frame-src should still be used
            //             for older browsers.
            filters.Add(
                new CspChildSrcAttribute()
                {
                    // Allow web workers or embed frames from example.com.
                    // CustomSources = "*.example.com",
                    // Allow web workers or embed frames from the same domain.
                    Self = false
                });
            // connect-src - This directive restricts which URIs the protected resource can load using script interfaces
            //               (Ajax Calls and Web Sockets).
            filters.Add(
                new CspConnectSrcAttribute()
                {
#if DEBUG
                    // Allow Browser Link to work in debug mode only.
                    CustomSources = string.Join(" ", "localhost:*", "ws://localhost:*"),
#else
                    // Allow AJAX and Web Sockets to example.com.
                    // CustomSources = "*.example.com",
#endif
                    // Allow all AJAX and Web Sockets calls from the same domain.
                    Self = true
                });
            // form-action - This directive restricts which URLs can be used as the action of HTML form elements.
            filters.Add(
                new CspFormActionAttribute()
                {
                    // Allow forms to post back to example.com.
                    // CustomSources = "*.example.com",
                    // Allow forms to post back to the same domain.
                    Self = true
                });
            // frame-src - This directive restricts from where the protected resource can embed frames.
            //             This is now deprecated in favour of child-src but should still be used for older browsers.
            filters.Add(
                new CspFrameSrcAttribute()
                {
                    // Allow iFrames from example.com.
                    // CustomSources = "*.example.com",
                    // Allow iFrames from the same domain.
                    Self = false
                });
            // frame-ancestors - This directive restricts from where the protected resource can embed frame, iframe,
            //                   object, embed or applet's.
            filters.Add(
                new CspFrameAncestorsAttribute()
                {
                    // Allow frame, iframe, object, embed or applet's from example.com.
                    // CustomSources = "*.example.com",
                    // Allow frame, iframe, object, embed or applet's from the same domain.
                    Self = false
                });
            // img-src - This directive restricts from where the protected resource can load images.
            filters.Add(
                new CspImgSrcAttribute()
                {
#if DEBUG
                    // Allow Browser Link to work in debug mode only.
                    CustomSources = "data:",
#else
                    // Allow images from example.com.
                    // CustomSources = "*.example.com",
#endif
                    // Allow images from the same domain.
                    Self = true,
                });
            // media-src - This directive restricts from where the protected resource can load video and audio.
            filters.Add(
                new CspMediaSrcAttribute()
                {
                    // Allow audio and video from example.com.
                    // CustomSources = "example.com",
                    // Allow audio and video from the same domain.
                    Self = false
                });
            // object-src - This directive restricts from where the protected resource can load plug-ins.
            filters.Add(
                new CspObjectSrcAttribute()
                {
                    // Allow plug-ins from example.com.
                    // CustomSources = "example.com",
                    // Allow plug-ins from the same domain.
                    Self = false
                });
            // plugin-types - This directive restricts the set of plug-ins that can be invoked by the protected resource.
            //                You can also use the @Html.CspMediaType("application/pdf") HTML helper instead of this
            //                attribute. The HTML helper will add the media type to the CSP header.
            // filters.Add(
            //     new CspPluginTypesAttribute()
            //     {
            //         // Allow Adobe Flash and Microsoft Silverlight plug-ins
            //         MediaTypes = "application/x-shockwave-flash application/xaml+xml"
            //     });
        }
    }
}
