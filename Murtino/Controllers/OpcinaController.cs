﻿using Murtino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Murtino.Controllers
{
    [Authorize]
    public class OpcinaController : Controller
    {
        public ActionResult Index(long? id)
        {
            UserModel user = Constants.CurrentUser;
            if (id.HasValue) return View(DB.Opcina(id.Value, user.StrankaID.Value));
            return View(new OpcinaModel());
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Upsert(OpcinaModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }

            UserModel user = Constants.CurrentUser;
            if (DB.Opcina(model, user.StrankaID.Value)) return RedirectToAction("Index", "Home");
            else
            {
                ModelState.AddModelError("", "Dodavanje stranke nije uspjelo.");
                return View("Index", model);
            }
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            UserModel user = Constants.CurrentUser;
            return Json(new { success = DB.DeleteOpcina(id, user.StrankaID.Value), redirect = Url.Action("Index", "Home") });
        }
    }
}
