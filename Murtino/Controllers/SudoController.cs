﻿using Murtino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Murtino.Controllers
{
    public class SudoController : Controller
    {
        public ActionResult Index()
        {
            return View(DB.Stranke());
        }

        public ActionResult Stranka(long id)
        {
            return View(DB.Stranka(id));
        }

        public ActionResult Create()
        {
            return View("Stranka", new StrankaViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upsert(StrankaViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Stranka", model);
            }

            if (DB.Stranka(model)) return RedirectToAction("Index");
            else
            {
                ModelState.AddModelError("", "Dodavanje stranke nije uspjelo.");
                return View("Stranka", model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(StrankaViewModel model)
        {
            DB.DeleteStranka(model);
            return RedirectToAction("Index");
        }
    }
}