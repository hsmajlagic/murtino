﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Murtino.Models
{
    public class GlasacModel
    {
        public long Id { get; set; }

        public long RedniBroj { get; set; }

        public DateTime? DatumRodjenja { get; set; }

        public string Ime { get; set; }

        public string ImeOca { get; set; }

        public string Prezime { get; set; }

        public string Naziv
        {
            get
            {
                string s = "";
                if (!String.IsNullOrWhiteSpace(this.Ime)) s += this.Ime;
                if (!String.IsNullOrWhiteSpace(this.ImeOca)) s += String.Format(" ({0})", this.ImeOca);
                if (!String.IsNullOrWhiteSpace(this.Prezime)) s += String.Format(" {0}", this.Prezime);
                return s;
            }
        }

        public bool Clan { get; set; }

        public bool Glasao { get; set; }

        public long OpcinaID { get; set; }

        public long? BirackoMjestoID { get; set; }
    }
}