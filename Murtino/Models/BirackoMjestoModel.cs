﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Murtino.Models
{
    public class BirackoMjestoModel
    {
        public long Id { get; set; }

        public string Naziv { get; set; }
    }
}