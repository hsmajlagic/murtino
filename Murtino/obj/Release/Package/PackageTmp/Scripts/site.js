﻿Vue.config.devtools = false;
Vue.config.productionTip = false;

(function () {
    var burger = document.querySelector('.burger');
    if (burger) {
        var menu = document.querySelector('#' + burger.dataset.target);
        burger.addEventListener('click', function () {
            burger.classList.toggle('is-active');
            menu.classList.toggle('is-active');
        });
    }
})();

document.querySelectorAll("#nav li").forEach(function (navEl) {
    navEl.onclick = function () { toggleTab(this.id, this.dataset.target); }
});

function toggleTab(selectedNav, targetId) {
    var navEls = document.querySelectorAll("#nav li");

    navEls.forEach(function (navEl) {
        if (navEl.id == selectedNav) {
            navEl.classList.add("is-active");
        } else {
            if (navEl.classList.contains("is-active")) {
                navEl.classList.remove("is-active");
            }
        }
    });

    var tabs = document.querySelectorAll(".tab-pane");
    tabs.forEach(function (tab) {
        if (tab.id == targetId) {
            tab.style.display = "block";
        } else {
            tab.style.display = "none";
        }
    });
};

$(document).ready(function () {

    $("#dodaj-opcinu").click(function (e) {
        e.preventDefault();
        var nazivBirackogMjesta = $("#biracko-mjesto").val();
        if (nazivBirackogMjesta) {
            $("#empty-row").remove();
            var count = $("#biracka-mjesta tr").length;
            var newId = _.max($("#biracka-mjesta .BirackoMjestoId").map(function () { return this.value; })) + 1;
            $("#biracka-mjesta").append($("<tr><td>" + nazivBirackogMjesta + "</td><td><a>Ukloni</a><input type='hidden' name='BirackaMjesta[" + count + "].Naziv' value='" + nazivBirackogMjesta + "' />" + "<input type='hidden' name='BirackaMjesta[" + count + "].Id' value='" + newId + "' />" + "</td></tr>"));
            $("#biracko-mjesto").val('');
        }
    });
    $("#biracka-mjesta").on("click", ".bm-remove", function (e) {
        $(e.currentTarget).closest('tr').remove();
    });
    $(".button-delete").click(function (e) {
        e.preventDefault();
        var $element = $(e.currentTarget);
        $.confirm({
            title: $element.data('title'),
            content: $element.data('content'),
            buttons: {
                confirm: {
                    text: 'Potvrdi',
                    btnClass: 'button is-link',
                    action: function () {
                        $.post($element.data('href'), { id: $element.data('id') }, function (response) {
                            if (response.success && response.redirect) {
                                window.location.href = response.redirect;
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Odustani',
                    btnClass: 'button is-danger'
                }
            }
        });
    });

    $("#import-glasaci").click(function (e) {
        e.preventDefault();
        var dialog = $.dialog({
            title: 'Import iz CSV',
            content: 'url:/glasac/import/',
            backgroundDismiss: true,
            onContentReady: function () {
                $("#opcinaId").change(function (e) {
                    var option = $(this).find(":selected");
                    var options = _.map(option.data('bm').split(','), function (item) {
                        var s = item.split(':');
                        return '<option value="' + s[0] + '">' + s[1] + '</option>';
                    });
                    $("#birackoMjestoId").empty().html(_.join(options, '')).show();
                });
                $("#csv").change(function () {
                    $("#import-confirm, #delimiter").show();
                    $("#odaberi-csv").text(document.getElementById('csv').value);
                });
                $("#odaberi-csv").click(function () {
                    $("#csv").click();
                });
                $("#import-glasaci-csv").show().submit(function (e) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    $.ajax({
                        url: $(this).attr("action"),
                        type: 'POST',
                        data: formData,
                        success: function (data) {
                            if (data) {
                                alert(data);
                            } else {
                                dialog.close();
                                window.location.reload();
                            }
                            // reload glasaci datasource
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                });
            }
        });
    });

    var getBirackoMjesto = function (glasac) {
        var o = _.find(glasacModel.opcine, function (item) { return item.Id == glasac.OpcinaID });
        if (o && glasac.BirackoMjestoID != null) {
            let bm = _.find(o.BirackaMjesta, function (item) { return item.Id == glasac.BirackoMjestoID });
            if (bm) {
                return bm.Naziv;
            }
        }
        return "";
    };

    $("#pdf-export").click(function (e) {
        e.preventDefault();
        var columns = ["Redni broj", "Ime i prezime", "B mjesto", "Datum rodjenja", "Glas", "Clan"];
        var rows = glasacModel.glasaci.map(function (item) { return [item.RedniBroj, item.Naziv, getBirackoMjesto(item), item.DR, item.Glasao ? "DA" : "NE", item.Clan ? "DA" : "NE"]; });
        var doc = new jsPDF('p', 'pt');
        doc.addFont('Lato-Regular.ttf', 'Lato', 'normal');
        doc.setFont("Lato");
        doc.setFontSize(12);
        doc.text('Ukupno: ' + glasacModel.glasaci.length + '  |  Glasali: ' + glasacModel.glasaci.filter(function (item) { return item.Glasao; }).length, 40, 35);
        doc.autoTable(columns, rows, { styles: { font: "Lato" } });
        doc.save($("#opcina-naziv option:selected").text() + " - " + ($("#bm-naziv option:selected").text() || 'biracka') + '.pdf');
    });

    $("select[name='OpcinaID']").change(function (e) {
        var option = $(this).find(":selected");
        var options = _.map(option.data('bm').split(','), function (item) {
            var s = item.split(':');
            return '<option value="' + s[0] + '">' + s[1] + '</option>';
        });
        $("select[name='BirackoMjestoID']").empty().html(_.join(options, '')).show();
    });

    function calculateAge(birthday) {
        if (!birthday) return null;
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    };

    if (document.getElementById("main")) {
        var strankaHub = $.connection.strankaHub;
        strankaHub.client.updateGlasac = function (glasac) {
            var index = _.findIndex(glasacModel.items, function (item) {
                return item.Id == glasac.Id;
            });
            if (index !== -1) {
                Vue.set(glasacModel.items, index, glasac);
                $("#status-" + glasac.Id).closest('tr').removeClass('updated');
                setTimeout(function () {
                    $("#status-" + glasac.Id).closest('tr').removeClass('updated').addClass('updated');
                }, 100);
            }
        };
        strankaHub.client.updateStranka = function (property, value) {
            Vue.set(vm.items, indexOfItem, newValue)
        };
        $.connection.hub.start().done(function () {
            strankaHub.server.subscribeStranka();
        });
        var glasaci = $("#glasaci").data("json");
        glasaci.forEach(function (item) {
            if (item.DatumRodjenja) {
                var d = new Date(item.DatumRodjenja);
                var day = d.getDate();
                var month = d.getMonth() + 1;
                if (day < 10) {
                    day = "0" + d.getDate();
                }
                if (month < 10) {
                    month = "0" + eval(d.getMonth() + 1);
                }
                item.Age = calculateAge(d);
                item.DR = day + "." + month + "." + d.getFullYear();
            }
        });
        var glasacModel = new Vue({
            el: '#glasaci',
            data: {
                keyword: '',
                status: '',
                clan: '',
                opcina: '',
                birackoMjesto: '',
                starost1: '',
                starost2: '',
                items: glasaci,
                opcine: $("#glasaci").data("opcine"),
                pageNumber: 0,
                size: 50
            },
            computed: {
                glasaci: function () {
                    var keyword = this.keyword;
                    var status = this.status;
                    var clan = this.clan;
                    var opcina = this.opcina;
                    var birackoMjesto = this.birackoMjesto;
                    var starost1 = this.starost1;
                    var starost2 = this.starost2;
                    var result = this.items;
                    if (keyword && (keyword.length > 1 || Number(keyword))) {
                        var split = keyword.split(",");
                        if (split && split.length > 0) {
                            var filter = [];
                            var items = this.items;
                            split.forEach(function (val) {
                                if (val) {
                                    val = val.toLowerCase();
                                    let f = items.filter(function (item) {
                                        return item.RedniBroj == val || item.Naziv && item.Naziv.toLowerCase().includes(val);
                                    });
                                    filter.push(...f);
                                }
                            });
                            result = filter;
                        } else {
                            keyword = keyword.toLowerCase();
                            result = result.filter(function (item) {
                                return item.RedniBroj == keyword || item.Naziv && item.Naziv.toLowerCase().includes(keyword);
                            });
                        }
                    }
                    if (status) {
                        status = status === 'true';
                        result = result.filter(function (item) {
                            return item.Glasao === status;
                        });
                    }
                    if (clan) {
                        clan = clan === 'true';
                        result = result.filter(function (item) {
                            return item.Clan === clan;
                        });
                    }
                    if (opcina) {
                        result = result.filter(function (item) {
                            return item.OpcinaID == opcina;
                        });
                    }
                    if (birackoMjesto !== '') {
                        result = result.filter(function (item) {
                            return item.BirackoMjestoID == birackoMjesto;
                        });
                    }
                    if (starost1 !== '') {
                        starost1 = Number(starost1);
                        result = result.filter(function (item) {
                            return item.Age != null && item.Age >= starost1;
                        });
                    }
                    if (starost2 !== '') {
                        starost2 = Number(starost2);
                        result = result.filter(function (item) {
                            return item.Age != null && item.Age <= starost2;
                        });
                    }
                    this.pageNumber = 0;
                    return result;
                },
                glasaciG: function () {
                    var glasaci = this.glasaci;
                    return glasaci.filter(function (item) { return item.Glasao; }).length;
                },
                birackaMjesta: function () {
                    var opcina = this.opcina;
                    if (opcina) {
                        var o = _.find(this.opcine, function (item) { return item.Id == opcina; });
                        if (o && o.BirackaMjesta && o.BirackaMjesta.length > 0) {
                            return o.BirackaMjesta;
                        }
                    }
                    return [];
                },
                popupContent: function () {
                    var glasaci = this.glasaci;
                    return "Ovime ćete potvrditi glas svim izlistanim glasačima (<b>" + glasaci.length + "</b>). Da li ste sigurni ?";
                },
                pageCount: function () {
                    let l = this.glasaci.length, s = this.size;
                    return Math.ceil(l / s);
                },
                paginatedData: function () {
                    const start = this.pageNumber * this.size, end = start + this.size;
                    return this.glasaci.slice(start, end);
                }
            },
            methods: {
                nextPage: function () {
                    this.pageNumber++;
                },
                prevPage: function () {
                    this.pageNumber--;
                },
                statusChanged: function (e, item) {
                    $.post('/glasac/upsert/', item);
                },
                clanChanged: function (e, item) {
                    $.post('/glasac/upsert/', item);
                },
                potvrdiGlas: function () {
                    var content = this.popupContent;
                    var glasaci = this.glasaci;
                    $.confirm({
                        title: "Potvrdi glas",
                        content: content,
                        buttons: {
                            confirm: {
                                text: 'Potvrdi',
                                btnClass: 'button is-link',
                                action: function () {
                                    glasaci.forEach(function (item) {
                                        if (!item.Glasao) {
                                            item.Glasao = true;
                                            $.post('/glasac/upsert/', item);
                                        }
                                    });
                                }
                            },
                            cancel: {
                                text: 'Odustani',
                                btnClass: 'button is-danger'
                            }
                        }
                    });
                }
            }
        });
        if (document.getElementById('posmatraci')) {
            var posmatracModel = new Vue({
                el: '#posmatraci',
                data: {
                    keyword: '',
                    opcina: '',
                    birackoMjesto: '',
                    items: $("#posmatraci").data("json"),
                    opcine: $("#posmatraci").data("opcine")
                },
                computed: {
                    posmatraci: function () {
                        var keyword = this.keyword;
                        var opcina = this.opcina;
                        var birackoMjesto = this.birackoMjesto;
                        var result = this.items;
                        if (keyword && (keyword.length > 1)) {
                            keyword = keyword.toLowerCase();
                            result = result.filter(function (item) {
                                return item.Naziv && item.Naziv.toLowerCase().includes(keyword);
                            });
                        }
                        if (opcina) {
                            result = result.filter(function (item) {
                                return item.OpcinaID == opcina;
                            });
                        }
                        if (birackoMjesto !== '') {
                            result = result.filter(function (item) {
                                return item.BirackoMjestoID == birackoMjesto;
                            });
                        }
                        return result;
                    },
                    birackaMjesta: function () {
                        var opcina = this.opcina;
                        if (opcina) {
                            var o = _.find(this.opcine, function (item) { return item.Id == opcina; });
                            if (o && o.BirackaMjesta && o.BirackaMjesta.length > 0) {
                                return o.BirackaMjesta;
                            }
                        }
                        return [];
                    }
                },
                methods: {
                    getOpcinaNaziv: function (posmatrac) {
                        var o = _.find(this.opcine, function (item) { return item.Id == posmatrac.OpcinaID });
                        if (o && posmatrac.BirackoMjestoID != null) {
                            let bm = _.find(o.BirackaMjesta, function (item) { return item.Id == posmatrac.BirackoMjestoID });
                            if (bm) {
                                posmatrac.BirackoMjesto = bm.Naziv;
                            }
                        }
                        return o != null ? o.Naziv : "";
                    }
                }
            });
        }
        setTimeout(function () {
            $("#glasaci").css("opacity", "1");
        }, 500);

        $("canvas").each(function () {
            $(this).attr('width', this.clientWidth);
            $(this).attr('height', this.height);
        });
        new Chart(document.getElementById("glasaciPieChart"), {
            type: 'pie',
            data: {
                labels: [
                    'Članovi koji su glasali',
                    'Članovi koji nisu glasali',
                    'Ostali glasači'
                ],
                datasets: [
                    {
                        label: "Population (millions)",
                        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
                        data: $("#glasaciPieChart").data('json')
                    }
                ]
            },
            options: {
                layout: {
                    padding: 30
                }
            }
        });
        new Chart(document.getElementById("useriPieChart"), {
            type: 'horizontalBar',
            data: {
                labels: ["Broj glasača", "Broj članova", "Broj posmatrača"],
                datasets: [
                    {
                        backgroundColor: ["#3cba9f", "#e8c3b9", "#c45850"],
                        data: $("#useriPieChart").data('json')
                    }
                ]
            },
            options: {
                legend: { display: false },
                layout: {
                    padding: 30
                }
            }
        });
        var ops = $("#opcinePieChart").data('json');
        new Chart(document.getElementById("opcinePieChart"), {
            type: 'bar',
            data: {
                labels: ops.map(function (item) { return item.Item3 }),
                datasets: [
                    {
                        label: "Glasali",
                        backgroundColor: "#3cba9f",
                        data: ops.map(function (item) { return item.Item1 })
                    }, {
                        label: "Nisu glasali",
                        backgroundColor: "#8e5ea2",
                        data: ops.map(function (item) { return item.Item2 })
                    }
                ]
            },
            options: {
                legend: { display: false },
                layout: {
                    padding: 30
                }
            }
        });
    }
});