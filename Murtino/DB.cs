﻿using LiteDB;
using Microsoft.AspNet.Identity.Owin;
using Murtino.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Murtino
{
    public static class DB
    {
        private static string Path(string s)
        {
            string path = System.IO.Path.Combine(Constants.PATH, Constants.PROD ? "DB" : "_DB");
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            return System.IO.Path.Combine(path, s);
        }

        public static List<StrankaModel> Stranke()
        {
            using (var db = new LiteDatabase(Path("stranke.db")))
            {
                var stranke = db.GetCollection<StrankaModel>("stranke");
                stranke.EnsureIndex(x => x.Id);
                stranke.EnsureIndex(x => x.Skracenica, true);
                stranke.EnsureIndex(x => x.Allowed);
                return stranke.FindAll().ToList();
            }
        }

        public static StrankaViewModel Stranka(string skracenica, UserModel user)
        {
            StrankaViewModel stranka = null;
            using (var db = new LiteDatabase(Path("stranke.db")))
            {
                var stranke = db.GetCollection<StrankaModel>("stranke");
                var s = String.IsNullOrEmpty(skracenica) ? stranke.FindOne(o => o.Id == user.StrankaID.Value) : stranke.FindOne(o => o.Skracenica == skracenica);
                if (s != null && s.Allowed)
                {
                    using (var db2 = new LiteDatabase(Path(s.Id + ".db")))
                    {
                        var users = db2.GetCollection<UserModel>("users");
                        var currentUser = users.FindOne(o => o.Id == user.Id && o.UserName == user.UserName);
                        if (currentUser != null)
                        {
                            currentUser.StrankaID = s.Id;
                            if (currentUser.Administrator)
                            {
                                var opcine = db2.GetCollection<OpcinaModel>("opcine");
                                var glasaci = db2.GetCollection<GlasacModel>("glasaci");
                                stranka = new StrankaViewModel(users.FindAll(), opcine.FindAll(), glasaci.FindAll(), s, currentUser);
                            }
                            else
                            {
                                var opcine = db2.GetCollection<OpcinaModel>("opcine");
                                var glasaci = db2.GetCollection<GlasacModel>("glasaci");
                                stranka = new StrankaViewModel(null, new List<OpcinaModel>() { opcine.FindOne(o => o.Id == currentUser.OpcinaID.Value) }, glasaci.Find(o => o.BirackoMjestoID == user.BirackoMjestoID), s, currentUser);
                            }
                        }
                    }
                }
            }
            return stranka;
        }

        public static StrankaViewModel Stranka(long id)
        {
            using (var db = new LiteDatabase(Path(id + ".db")))
            {
                var users = db.GetCollection<UserModel>("users");
                users.EnsureIndex(x => x.Id);
                users.EnsureIndex(x => x.UserName);
                users.EnsureIndex(x => x.BirackoMjestoID);
                var opcine = db.GetCollection<OpcinaModel>("opcine");
                opcine.EnsureIndex(x => x.Id);
                opcine.EnsureIndex(x => x.BirackaMjesta, "$.BirackaMjesta[*]");
                var glasaci = db.GetCollection<GlasacModel>("glasaci");
                glasaci.EnsureIndex(x => x.Ime);
                glasaci.EnsureIndex(x => x.Prezime);
                glasaci.EnsureIndex(x => x.Glasao);
                glasaci.EnsureIndex(x => x.RedniBroj);
                glasaci.EnsureIndex(x => x.OpcinaID);
                glasaci.EnsureIndex(x => x.BirackoMjestoID);
                using (var db2 = new LiteDatabase(Path("stranke.db")))
                {
                    var stranke = db2.GetCollection<StrankaModel>("stranke");
                    var s = stranke.FindById(id);
                    return new StrankaViewModel(users.FindAll(), opcine.FindAll(), glasaci.FindAll(), s);
                }
            }
        }

        public static bool Stranka(StrankaViewModel model)
        {
            using (var db = new LiteDatabase(Path("stranke.db")))
            {
                var stranke = db.GetCollection<StrankaModel>("stranke");
                var stranka = stranke.FindById(model.Stranka.Id);
                if (stranka != null)
                {
                    stranka.Naziv = model.Stranka.Naziv;
                    stranka.Skracenica = model.Stranka.Skracenica;
                    stranka.Allowed = model.Stranka.Allowed;
                    stranke.Update(stranka);
                }
                else
                {
                    model.Stranka.Id = stranke.Insert(model.Stranka);
                }
            }
            if (model.Stranka.Id != 0)
            {
                using (var db = new LiteDatabase(Path(model.Stranka.Id + ".db")))
                {
                    var users = db.GetCollection<UserModel>("users");

                    var administrator = users.FindOne(o => o.Administrator);
                    if (administrator != null)
                    {
                        administrator.UserName = model.Administrator.UserName;
                        administrator.Password = Constants.Hash256(model.Administrator.Password);
                        administrator.Ime = model.Administrator.Ime;
                        administrator.Prezime = model.Administrator.Prezime;
                        administrator.StrankaID = model.Stranka.Id;
                        administrator.Administrator = true;
                        users.Update(administrator);
                        model.Administrator.Id = administrator.Id;
                    }
                    else
                    {
                        model.Administrator.StrankaID = model.Stranka.Id;
                        model.Administrator.Administrator = true;
                        model.Administrator.Password = Constants.Hash256(model.Administrator.Password);
                        model.Administrator.Id = users.Insert(model.Administrator);
                    }
                }
            }
            return model.Stranka.Id != 0 && model.Administrator.Id != 0;
        }

        public static void DeleteStranka(StrankaViewModel model)
        {
            using (var db = new LiteDatabase(Path("stranke.db")))
            {
                var stranke = db.GetCollection<StrankaModel>("stranke");
                stranke.Delete(model.Stranka.Id);
            }
            if (model.Stranka.Id != 0)
            {
                using (var db = new LiteDatabase(Path(model.Stranka.Id + ".db")))
                {
                    db.DropCollection("users");
                    db.DropCollection("opcine");
                    db.DropCollection("glasaci");
                }
            }
        }

        public static SignInStatus Login(string username, string password, out StrankaViewModel model)
        {
            password = Constants.Hash256(password);
            using (var db = new LiteDatabase(Path("stranke.db")))
            {
                var stranke = db.GetCollection<StrankaModel>("stranke");
                var allStranke = stranke.FindAll().ToList();
                for (int i = 0; i < allStranke.Count; i++)
                {
                    var stranka = allStranke[i];
                    using (var db2 = new LiteDatabase(Path(stranka.Id + ".db")))
                    {
                        var users = db2.GetCollection<UserModel>("users");
                        var user = users.FindOne(o => o.UserName == username && o.Password == password);
                        if (user != null)
                        {
                            user.StrankaID = stranka.Id;
                            if (user.Administrator)
                            {
                                var opcine = db2.GetCollection<OpcinaModel>("opcine");
                                var glasaci = db2.GetCollection<GlasacModel>("glasaci");
                                model = new StrankaViewModel(users.FindAll(), opcine.FindAll(), glasaci.FindAll(), stranka);
                            }
                            else
                            {
                                var opcine = db2.GetCollection<OpcinaModel>("opcine");
                                var glasaci = db2.GetCollection<GlasacModel>("glasaci");
                                model = new StrankaViewModel(null, new List<OpcinaModel>() { opcine.FindOne(o =>o.Id == user.OpcinaID.Value) }, glasaci.Find(o => o.BirackoMjestoID == user.BirackoMjestoID), stranka, user);
                            }
                            return SignInStatus.Success;
                        }
                    }
                }
                model = null;
                return SignInStatus.Failure;
            }
        }

        public static IEnumerable<OpcinaModel> Opcine(long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var opcine = db.GetCollection<OpcinaModel>("opcine");
                return opcine.FindAll().OrderBy(o => o.Naziv);
            }
        }

        public static OpcinaModel Opcina(long id, long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var opcine = db.GetCollection<OpcinaModel>("opcine");
                return opcine.FindById(id);
            }
        }

        public static bool Opcina(OpcinaModel model, long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var opcine = db.GetCollection<OpcinaModel>("opcine");
                if (model.Id != 0) return opcine.Update(model);
                return opcine.Insert(model) != 0;
            }
        }

        public static bool DeleteOpcina(long id, long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var opcine = db.GetCollection<OpcinaModel>("opcine");
                return opcine.Delete(id);
            }
        }

        public static GlasacModel Glasac(long id, long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var glasaci = db.GetCollection<GlasacModel>("glasaci");
                return glasaci.FindById(id);
            }
        }

        public static bool Glasac(GlasacModel model, long strankaId)
        {
            bool result = false;
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var glasaci = db.GetCollection<GlasacModel>("glasaci");
                if (model.Id != 0) result = glasaci.Update(model);
                else result = glasaci.Insert(model) != 0;
            }
            if (result)
            {
                var hubContext = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<StrankaHub>();
                hubContext.Clients.Group(strankaId.ToString()).updateGlasac(model);
            }
            return result;
        }

        public static bool DeleteGlasac(long id, long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var glasaci = db.GetCollection<GlasacModel>("glasaci");
                return glasaci.Delete(id);
            }
        }

        public static void ImportGlasaci(List<GlasacModel> records, long strankaId, long opcinaId, long birackoMjestoId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var glasaci = db.GetCollection<GlasacModel>("glasaci");
                glasaci.Delete(g => g.OpcinaID == opcinaId && g.BirackoMjestoID == birackoMjestoId);
                glasaci.EnsureIndex(x => x.Ime);
                glasaci.EnsureIndex(x => x.Prezime);
                glasaci.EnsureIndex(x => x.Glasao);
                glasaci.EnsureIndex(x => x.RedniBroj);
                glasaci.EnsureIndex(x => x.OpcinaID);
                glasaci.EnsureIndex(x => x.BirackoMjestoID);
                glasaci.InsertBulk(records);
            }
        }

        public static UserModel Posmatrac(long id, long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var posmatraci = db.GetCollection<UserModel>("users");
                return posmatraci.FindById(id);
            }
        }

        public static bool Posmatrac(UserModel model, long strankaId)
        {
            bool result = false;
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                model.Administrator = false;
                model.StrankaID = strankaId;
                model.Password = Constants.Hash256(model.Password);
                var users = db.GetCollection<UserModel>("users");
                if (model.Id != 0) result = users.Update(model);
                else result = users.Insert(model) != 0;
            }
            return result;
        }

        public static bool DeletePosmatrac(long id, long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var users = db.GetCollection<UserModel>("users");
                return users.Delete(id);
            }
        }

        public static void ImportPosmatraci(List<UserModel> records, long strankaId)
        {
            using (var db = new LiteDatabase(Path(strankaId + ".db")))
            {
                var users = db.GetCollection<UserModel>("users");
                db.DropCollection("users");
                users.EnsureIndex(x => x.UserName);
                users.EnsureIndex(x => x.BirackoMjestoID);
                users.InsertBulk(records);
            }
        }
    }
}