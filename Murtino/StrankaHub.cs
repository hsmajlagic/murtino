﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Murtino
{
    public class StrankaHub : Hub
    {
        public async Task SubscribeStranka()
        {
            await Groups.Add(Context.ConnectionId, Constants.CurrentUser.StrankaID.Value.ToString());
        }
    }
}