﻿using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace Murtino.Models
{
    public class UserModel : IUser<long>
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Unesite Vaše korisničko ime!")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Korisničko ime mora imati format email adrese")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Unesite Vaše lozinku!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //[Required(ErrorMessage = "Ime je obavezno")]
        //[DataType(DataType.Text)]
        public string Ime { get; set; }

        //[Required(ErrorMessage = "Prezime je obavezno")]
        //[DataType(DataType.Text)]
        public string Prezime { get; set; }

        public string Naziv
        {
            get
            {
                return String.Format("{0} {1}", this.Ime, this.Prezime);
            }
        }

        public string Telefon { get; set; }

        public string Lokacija { get; set; }

        public long? OpcinaID { get; set; }

        public long? BirackoMjestoID { get; set; }

        public long? StrankaID { get; set; }

        public bool Administrator { get; set; }

        public bool RememberMe { get; set; } = true;
    }
}