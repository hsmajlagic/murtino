﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Murtino.Models
{
    public class StrankaModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Unesite puni naziv stranke")]
        [MinLength(5, ErrorMessage = "Naziv mora sadržavati miinimalno 5 karaktera")]
        public string Naziv { get; set; }

        [Required(ErrorMessage = "Unesite skraćenicu stranke")]
        [MinLength(2, ErrorMessage = "Skraćenica mora sadržavati miinimalno 2 karaktera")]
        public string Skracenica { get; set; }

        public bool Allowed { get; set; } = true;
    }
}