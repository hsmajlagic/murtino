﻿using CsvHelper;
using Murtino.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Murtino.Controllers
{
    public class GlasacController : Controller
    {
        public ActionResult Index(long? id)
        {
            UserModel user = Constants.CurrentUser;
            ViewBag.Opcine = DB.Opcine(user.StrankaID.Value);
            if (id.HasValue) return View(DB.Glasac(id.Value, user.StrankaID.Value));
            return View(new GlasacModel());
        }

        [HttpPost]
        public ActionResult Upsert(GlasacModel model)
        {
            UserModel user = Constants.CurrentUser;

            if (!ModelState.IsValid)
            {
                ViewBag.Opcine = DB.Opcine(user.StrankaID.Value);
                return View("Index", model);
            }

            if (DB.Glasac(model, user.StrankaID.Value))
            {
                if (Request.IsAjaxRequest()) return Json(new { success = true });
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Opcine = DB.Opcine(user.StrankaID.Value);
                ModelState.AddModelError("", "Dodavanje glasača nije uspjelo.");
                return View("Index", model);
            }
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            UserModel user = Constants.CurrentUser;
            return Json(new { success = DB.DeleteGlasac(id, user.StrankaID.Value), redirect = Url.Action("Index", "Home") });
        }

        [HttpGet]
        public ActionResult Import()
        {
            return PartialView("_ImportGlasaci", Session[Session.SessionID] as StrankaViewModel);
        }

        [HttpPost]
        public ActionResult Import(long opcinaId, long birackoMjestoId, string delimiter)
        {
            HttpFileCollectionBase files = Request.Files;
            if (files != null && files.Count > 0)
            {
                try
                {
                    var file = files[0];
                    if (file.ContentLength == 0) return Content("File empty.");

                    using (var reader = new StreamReader(file.InputStream, System.Text.Encoding.UTF8))
                    {
                        CsvReader csv = new CsvReader(reader);
                        csv.Configuration.Delimiter = String.IsNullOrWhiteSpace(delimiter) ? ";" : delimiter;
                        csv.Configuration.Encoding = System.Text.Encoding.UTF8;
                        csv.Configuration.RegisterClassMap<GlasacClassMap>();
                        var records = csv.GetRecords<GlasacModel>().ToList();
                        UserModel user = Constants.CurrentUser;
                        records.ForEach(o =>
                        {
                            o.Ime = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(o.Ime.ToLower());
                            o.ImeOca = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(o.ImeOca.ToLower());
                            o.Prezime = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(o.Prezime.ToLower());
                            o.OpcinaID = opcinaId;
                            o.BirackoMjestoID = birackoMjestoId;
                        });
                        DB.ImportGlasaci(records, user.StrankaID.Value, opcinaId, birackoMjestoId);
                        return new EmptyResult();
                    }
                }
                catch (Exception ex)
                {
                    return Content(ex.Message);
                }
                
            }
            return Content("File upload failed.");
        }
    }
}