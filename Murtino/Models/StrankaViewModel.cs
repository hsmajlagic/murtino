﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Murtino.Models
{
    public class StrankaViewModel
    {
        public StrankaModel Stranka { get; set; }

        public UserModel Administrator { get; set; }

        public List<UserModel> Users { get; set; }

        public List<OpcinaModel> Opcine { get; set; }

        public List<GlasacModel> Glasaci { get; set; }

        public StrankaViewModel(IEnumerable<UserModel> users, IEnumerable<OpcinaModel> opcine, IEnumerable<GlasacModel> glasaci, StrankaModel stranka, UserModel user = null)
        {
            this.Users = users != null ? users.Where(o => !o.Administrator).ToList() : new List<UserModel>();
            this.Opcine = opcine != null ? opcine.OrderBy(o => o.Naziv).ToList() : new List<OpcinaModel>();
            this.Glasaci = glasaci.ToList();
            this.Stranka = stranka;
            this.Administrator = user ?? users.FirstOrDefault(o => o.Administrator);
        }

        public StrankaViewModel()
        {
            this.Administrator = new UserModel();
            this.Stranka = new StrankaModel();
            this.Users = new List<UserModel>();
            this.Opcine = new List<OpcinaModel>();
            this.Glasaci = new List<GlasacModel>();
        }
    }
}