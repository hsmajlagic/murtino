http://hsmajlagic1-001-site1.btempurl.com/sudo/ 
- Početna administracija za dodavanje stranke i administratorskog usera te stranke
- Za sad  nije zaštićeno, svako može pristupiti i bez logina, ali URL nije nigdje dostupan, dodat ćemo specijalnu autentifikaciju samo za taj URL
- Svaka kreirana stranka mora imati administratora s kojim se dalje uloguje u sistem i dodaju ostali podaci
- Stranke ne bi trebale imati administratore sa istim username-om, jer je login centraliziran i prilikom logiranja se pretražuju redom korisnici svih stranaka dok se ne nađe prvi sa datim username-om (i onda provjerava password)

http://hsmajlagic1-001-site1.btempurl.com/login/
- Početni ekran za sve korisnike, standardni login sa remember me funkcijom koju moram provjeriti jer ne radi
- Link za pomoć samo otvara defaultni email klijent, support email je trenutno haris_smajlagic@hotmail.com

http://hsmajlagic1-001-site1.btempurl.com/
- Glavna stranica za sve korisnike, sadrži 2 ili 4 taba ovisno da li je korisnik administrator ili ne
- Administrator ima 4 taba i može dodavati i mijenjati glasače, posmatrače i općine
- Prvo uvijek treba dodati općinu i njena biračka mjesta, jer su glasači i posmatrači vezani na te dvije lokacije
- Nakon što je prisutna barem jedna općina, mogu se dodavati i glasači i posmatrači
- Ima opcija importovanja glasača iz CSV fajla kao što je dat primjer na viberu, s tim što importovanje prvo pobriše sve glasače pa ih ubaci, neophodno je prilikom importa definisati u koju općinu i biračko mjesto idu glasači [ovo možemo još dogovoriti]
- Editovanje statusa glasača kao i članstvo u stranci se može mijenjati direktno na listi i te promjene se manifestuju real-time (u drugom browseru se odmah vidi update kad se označi da je neko glasao)
- Filtriranje je direktno na listi, prema nekoj od opcija iz menija, kao i prema ključnoj riječi (minimalno 2 karaktera)
- Posmatrač vidi samo listu glasača sa općine i biračkog mjesta kojem pripada, može im jedino mijenjati status da li je glasao ili ne
- Administrator ima uvid u statistiku na nivou cijele stranke
- Statistika je trenutno relativno jednostavna i može se dalje dorađivati i dodavati, ja sam napravio samo par osnovnih dijagrama i chartova prema dostupnim podacima u trenutnom sistemu
- Statistika se treba ažurirati također real-time, ali trenutno nije implementirano
- Nije implementirana profilna stranica korisnika (u meniju -> Postavke) u kojoj bi mogao mijenjati svoje profilne podatke
- Nije implementiran generalni error handling i real-time notifikacijski popup
- Mnoštvo UI sitnica koje su u međuvremenu dodatno srediti
- Nije implementirana paginacija liste glasača i posmatrača