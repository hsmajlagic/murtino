﻿using Microsoft.Owin.Security;
using Murtino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Murtino
{
    public static class Constants
    {
        public static readonly bool PROD = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PROD"]);

        public static readonly string PATH = System.Web.Hosting.HostingEnvironment.MapPath("~/");

        public static readonly string SUPPORT_EMAIL = "haris_smajlagic@hotmail.com";

        public static string AppDataPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data");

        public static string Path
        {
            get
            {
                return System.IO.Path.Combine(AppDataPath, System.Web.HttpContext.Current.Session.SessionID);
            }
        }

        public static string Hash256(string value)
        {
            StringBuilder Sb = new StringBuilder();
            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));
                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }
            return Sb.ToString();
        }

        public static UserModel CurrentUser
        {
            get
            {
                IAuthenticationManager auth = System.Web.HttpContext.Current.GetOwinContext().Authentication;
                if (auth == null && auth.User == null) return null;
                UserModel user = new UserModel() { UserName = auth.User.Identity.Name, Ime = auth.User.FindFirst("Ime").Value, Prezime = auth.User.FindFirst("Prezime").Value, Administrator = auth.User.HasClaim("Administrator", "Administrator") };
                var userID = auth.User.FindFirst("Id");
                if (userID != null) user.Id = Convert.ToInt64(userID.Value);
                var birackoMjestoID = auth.User.FindFirst("BirackoMjestoID");
                if (birackoMjestoID != null) user.BirackoMjestoID = Convert.ToInt64(birackoMjestoID.Value);
                var strankaID = auth.User.FindFirst("StrankaID");
                if (strankaID != null) user.StrankaID = Convert.ToInt64(strankaID.Value);
                return user;
            }
        }
    }
}