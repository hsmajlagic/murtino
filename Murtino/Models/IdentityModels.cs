﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Murtino.Models
{
    public interface IUserService
    {
        UserModel Authenticate(string username, string password);
        UserModel GetUserByUsername(string username);
    }

    public class UserService : IUserService
    {
        public UserModel Authenticate(string username, string password)
        {
            return new UserModel() { UserName = username, Password = password };
        }

        public UserModel GetUserByUsername(string username)
        {
            return new UserModel() { UserName = username };
        }
    }

    public class SimpleUserStore<T> : IUserStore<T, long> where T : UserModel
    {
        private readonly IUserService _userService;

        public SimpleUserStore(IUserService userService)
        {
            _userService = userService;
        }

        public Task CreateAsync(T user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(T user)
        {
            throw new NotImplementedException();
        }

        public Task<T> FindByIdAsync(long userId)
        {
            throw new NotImplementedException();
        }

        public Task<T> FindByNameAsync(string userName)
        {
            var task = Task<T>.Run(() =>
            {
                return (T)_userService.GetUserByUsername(userName);
            });

            return task;
        }

        public Task UpdateAsync(T user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    public interface ISimpleUserManager
    {
        Task<UserModel> FindAsync(string userName, string password);
        Task SignInAsync(UserModel user, bool isPersistent);
        void SignOut();
    }

    public class SimpleUserManager : UserManager<UserModel, long>, ISimpleUserManager
    {
        private readonly IUserService _userService;
        private readonly IAuthenticationManager _authenticationManager;

        public SimpleUserManager(IUserService userService, IAuthenticationManager authenticationManager)
            : base(new SimpleUserStore<UserModel>(userService))
        {
            _userService = userService;
            _authenticationManager = authenticationManager;
        }

        public async Task SignInAsync(UserModel user, bool isPersistent)
        {
            SignOut();

            var identity = await base.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim("Id", user.Id.ToString()));
            identity.AddClaim(new Claim("Ime", user.Ime));
            identity.AddClaim(new Claim("Prezime", user.Prezime));
            identity.AddClaim(new Claim("UserName", user.UserName));
            if (user.Administrator) identity.AddClaim(new Claim("Administrator", "Administrator"));
            if (user.StrankaID.HasValue) identity.AddClaim(new Claim("StrankaID", user.StrankaID.Value.ToString()));
            if (user.BirackoMjestoID.HasValue) identity.AddClaim(new Claim("BirackoMjestoID", user.BirackoMjestoID.Value.ToString()));
            _authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        public void SignOut()
        {
            _authenticationManager.SignOut();
        }
    }
}