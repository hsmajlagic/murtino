﻿using Murtino.Models;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace Murtino.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated) return RedirectToAction("Login", "Account");
            UserModel user = Constants.CurrentUser;
            StrankaViewModel model = TempData[Session.SessionID] as StrankaViewModel;
            if (model != null) TempData.Remove(Session.SessionID);
            else model = DB.Stranka(null, user);
            Session[Session.SessionID] = model;
            return View(model);
        }
    }
}