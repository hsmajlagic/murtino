﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Murtino.Startup))]
namespace Murtino
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
