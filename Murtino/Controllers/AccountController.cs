﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Murtino.Models;

namespace Murtino.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IAuthenticationManager _authManager;

        public AccountController()
        {
            _authManager = System.Web.HttpContext.Current.GetOwinContext().Authentication;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new UserModel());
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(UserModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            SignInStatus result = DB.Login(model.UserName, model.Password, out StrankaViewModel stranka);
            switch (result)
            {
                case SignInStatus.Success:
                    await new SimpleUserManager(_userService, _authManager).SignInAsync(stranka.Administrator, model.RememberMe);
                    TempData[Session.SessionID] = stranka;
                    if (!String.IsNullOrEmpty(returnUrl)) return RedirectToLocal(returnUrl);
                    return RedirectToAction("Index", "Home");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Vaša prijava nije uspjela.");
                    return View(model);
            }
        }

        public ActionResult Logout()
        {
            var manager = new SimpleUserManager(_userService, _authManager);
            manager.SignOut();

            return RedirectToAction("Login");
        }
       

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
        
        #endregion
    }
}