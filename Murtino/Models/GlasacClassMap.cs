﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Murtino.Models
{
    public sealed class GlasacClassMap : ClassMap<GlasacModel>
    {
        public GlasacClassMap()
        {
            Map(m => m.Id).Ignore();
            //Map(m => m.Clan).Ignore();
            Map(m => m.OpcinaID).Ignore();
            Map(m => m.BirackoMjestoID).Ignore();
            Map(m => m.RedniBroj).Name("Redni broj");
            Map(m => m.DatumRodjenja).ConvertUsing((row) => 
            {
                string[] ss = row.GetField("Datum")?.Split('.');
                return ss != null && ss.Length >= 3 ? new DateTime(Convert.ToInt32(ss[2]), Convert.ToInt32(ss[1]), Convert.ToInt32(ss[0])) : (DateTime?)null;
            }).Name("Datum");
            Map(m => m.Ime).Name("Ime");
            Map(m => m.ImeOca).Name("Ime oca");
            Map(m => m.Prezime).Name("Prezime");
            Map(m => m.Glasao).Name("Status");
            Map(m => m.Clan).ConvertUsing((row) =>
            {
                if (row.TryGetField<string>("Clan", out string clan))
                {
                    if (clan == "1") return true;
                }
                return false;
            });
        }
    }
}