﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Murtino.Models
{
    public class OpcinaModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Naziv općine je obavezan")]
        public string Naziv { get; set; }

        [Required(ErrorMessage = "Poštanski broj je obavezan")]
        public int PostanskiBroj { get; set; }

        private IEnumerable<BirackoMjestoModel> _BirackaMjesta;

        public List<BirackoMjestoModel> BirackaMjesta
        {
            get
            {
                return _BirackaMjesta.OrderBy(o => o.Naziv).ToList();
            }
            set
            {
                _BirackaMjesta = value;
            }
        }

        public OpcinaModel()
        {
            this.BirackaMjesta = new List<BirackoMjestoModel>();
        }
    }
}