﻿using System.Web;
using System.Web.Optimization;

namespace Murtino
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.signalR-2.3.0.js",
                        "~/Scripts/lodash.js",
                        "~/Scripts/vue.js",
                        "~/Scripts/jquery-confirm.js",
                        "~/Scripts/chartjs.js",
                        "~/Scripts/html2pdf.bundle.js",
                        "~/Scripts/site.js"
                        ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/normalize.css",
                      "~/Content/bulma.css",
                      "~/Content/jquery-confirm.css",
                      "~/Content/site.css"
                      ));
        }
    }
}
